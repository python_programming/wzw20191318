"""
作者：王泽文
文件名称：Python语言基础实践
时间：2020.3.4
"""

# 仿射加密
note1 = '''
加密函数：E(x) = (5x + 8) (mod 26)
解密函数：D(x) = 21(x - 8) (mod 26)
'''
print(note1)
x = int(input("输入一个x = "))
encrypt = (5 * x + 8) % 26
print("E(x) =", encrypt)
y = encrypt
decode1 = (21 * (y - 8)) % 26
print("D(y) =", decode1)

# 等比求和
print("等比求和")
a = int(input("输入首项:"))
q = int(input("输入公比:"))
n = int(input("输入项数:"))
i = 0
Sn = 0
'''
if q == 1:
    Sn = n*a
else:
    Sn = a*((q**n)-1)/(q-1)
'''
while i < n:
    Sn = Sn + a
    a = a * q
    i = i + 1
print("Sn =", Sn)
