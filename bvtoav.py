"""
    作者：王泽文
    文件名：B站bv号转av号
    时间：2020年5月27日
"""
a = '''
                  //     
      \ \        //    
       \ \      //    
##DDDDDDDDDDDDDDDDDDDDDD##    
## DDDDDDDDDDDDDDDDDDDD ##   ________   ___   ___        ___   ________    ___   ___         ___    
## hh                hh ##   |\   __  \ |\  \ |\  \      |\  \ |\   __  \  |\  \ |\  \       |\  \    
## hh    //    \ \   hh ##   \ \  \|\ /_\ \  \ \ \ \     \ \  \ \ \  \|\ /_ \ \ \ \ \ \      \ \  \    
## hh   //      \ \  hh ##    \ \   __ \ \ \  \ \ \ \     \ \  \ \ \   __  \ \ \ \ \ \ \      \ \  \    
## hh                hh ##     \ \  \|\ \ \ \  \ \ \ \____ \ \  \ \ \  \|\  \ \ \ \ \ \ \____  \ \  \    
## hh      wwww      hh ##      \ \______\ \ \__\ \ \_____\ \ \__\ \ \_______\ \ \_\ \ \______\ \ \__\    
## hh                hh ##       \|_______| \|__| \|_______| \|__|  \|_______|  \|__| \|_______| \|__|    
## MMMMMMMMMMMMMMMMMMMM ##    
##MMMMMMMMMMMMMMMMMMMMMM##                             Release 2.6.1. Powered by jinkela-core 2.5.4.         
     \/            \/
'''


def bv_to_av(bv):
    bv_data = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M",
               "N",
               "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
               "k",
               "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
    data = [13, 12, 46, 31, 43, 18, 40, 28, 5, 54, 20, 15, 8, 39, 57, 45, 36, 38, 51, 42, 49, 52, 53, 7, 4, 9, 50, 10,
            44, 34, 6, 25, 1, 26, 29, 56, 3, 24, 0, 47, 27, 22, 41, 16, 11, 37, 2, 35, 21, 17, 33, 30, 48, 23, 55, 32,
            14, 19]
    num0 = [6, 2, 4, 8, 5, 9, 3, 7, 1, 0]
    num1 = 100618342136696320
    num2 = 177451812
    bv_new = []
    for i in bv:
        bv_new.append(i)
    del bv_new[0]
    del bv_new[0]
    for i in range(len(bv_new)):
        for n in range(len(bv_data)):
            if bv_new[i] == bv_data[n]:
                bv_new[i] = data[n]
    data_sum = 0
    for i in range(len(bv_new)):
        bv_new[i] = bv_new[i] * (58 ** num0[i])
        data_sum = data_sum + bv_new[i]
    av_result = (data_sum - num1) ^ num2
    return av_result


def enc(x):
    alphabet = 'fZodR9XQDSUm21yCkr6zBqiveYah8bt4xsWpHnJE7jL5VG3guMTKNPAwcF'
    x = (x ^ 0x0a93_b324) + 0x2_0840_07c0
    r = list('BV1**4*1*7**')
    for v in [11, 10, 3, 8, 4, 6]:
        x, d = divmod(x, 58)
        r[v] = alphabet[d]
    return ''.join(r)


if __name__ == '__main__':
    # print(a)
    b = int(input("input av号转bv号:1 or bv号转av号:2:"))
    if b == 2:
        bv = input("input bv:")
        av = bv_to_av(bv)
        print("av is:", av)
    elif b == 1:
        bv = int(input("input av:"))
        av = enc(bv)
        print("bv is:", av)

    # city sky lines   pw:av20010302
#     epic pw:av121101
