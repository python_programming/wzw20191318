"""
    作者：王泽文
    文件名：socket实践
    时间：2020年5月6日
"""
import socket
import os.path

while True:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(('127.0.0.1', 800))
    a = int(input("1.传输数据 2.退出 3.文件\n请输入："))
    if a == 1:
        str = input("输入：")
        s.sendall(str.encode())
        data = s.recv(1024)
        print(data.decode())
    elif a == 3:
        ad = input("ad:")
        f0 = open(ad, "r+")
        str = f0.read()
        s.sendall(str.encode())
        data = s.recv(1024)
        print(data.decode())

    else:
        str = "对方已退出"
        s.sendall(str.encode())
        print("已退出")
        s.close()
        break

