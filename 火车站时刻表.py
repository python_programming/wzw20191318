"""
    作者：王泽文
    名称：火车时刻表
    时间：2020年3月28日
"""
t = ("车次", "出发站", "-", "到达站", "出发时间", "到达时间", "历时")
information = [("T40", "长春", "-", "北京", "00:12", "12:20", "12:08"),
               ("T298", "长春", "-", "北京", "00:06", "10:50", "10:44"),
               ("Z158", "长春", "-", "北京", "12:48", "21:06", "08:18"),
               ("Z62", "长春", "-", "北京", "21:58", "06:08", "08:20")]
while True:
    print("{:^70} ".format('\033[1;37m火车站时刻表\033[0m'))
    print("_" * 68)
    print("{:<5}{:^9}{:^10}{:^10}{:^7}{:^7}{:^7}".format(*t))
    print("_" * 68)
    l0 = len(information)
    for i in range(l0):
        if i % 2 == 0:
            print('\033[1;32m{:<7}{:^10}{:^10}{:^10}{:^10}{:^10}{:^10}\033[0m'.format(*information[i]))
        elif i % 2 == 1:
            print('\033[1;34m{:<7}{:^10}{:^10}{:^10}{:^10}{:^10}{:^10}\033[0m'.format(*information[i]))
        print("_" * 68)
    a = input("添加/删除/退出\n请输入：")
    if a == "添加":
        i1 = input("请输入车次：")
        i2 = input("请输入出发站：")
        i3 = input("请输入到达站：")
        i4 = input("请输入出发时间：")
        i5 = input("请输入到达时间：")
        i6 = input("请输入历时：")
        new = (i1, i2, "-", i3, i4, i5, i6)
        information.append(new)
        print("添加成功\n")
        continue
    elif a == "删除":
        name = input("请输入车次：")
        for i in range(l0):
            if name == information[i][0]:
                q = i
        del information[q]
        print("删除成功\n")
        continue
    elif a == "退出":
        break
