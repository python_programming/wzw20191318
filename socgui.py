import socket
import tkinter as tk
import tkinter.scrolledtext as tst
import time
import tkinter.messagebox
import threading


class Application(tk.Frame):
    def __init__(self, master):
        tk.Frame.__init__(self, master)
        self.grid()
        self.createWidgets()

    def createWidgets(self):
        self.textEdit = tst.ScrolledText(self, width=50, height=15)
        self.textEdit.grid(row=0, column=0, rowspan=1, columnspan=4, padx=20, pady=5)
        self.textEdit.tag_config('server', foreground='green')
        self.textEdit.tag_config('guest', foreground='blue')
        self.inputText = tk.Text(self, width=40, height=5)
        self.inputText.grid(row=1, column=0, columnspan=1, padx=20, pady=5)
        self.inputText.bind("<KeyPress-Return>", self.textSendReturn)
        self.btnSend = tk.Button(self, text='send', command=self.textSend)
        self.btnSend.grid(row=1, column=3)
        t = threading.Thread(target=self.getInfo)
        t.start()

    def textSend(self):
        str = self.inputText.get('1.0', 'end-1c')
        if str != "":
            timemsg = '服务端' + time.strftime('%Y-%m-%d %H:%M:%S', time.localtime()) + '\n'
            self.textEdit.config(state='normal')
            self.textEdit.insert(tk.END, timemsg, 'server')
            self.textEdit.insert(tk.END, str + '\n')
            self.textEdit.see(tk.END)
            self.textEdit.config(state='disabled')
            self.inputText.delete(0.0, tk.END)
            sendMessage = bytes(str, encoding='utf8')
            connectionSocket.send(sendMessage)
        else:
            tk.messagebox.showinfo('警告', "不能发送空白信息！")

    def getInfo(self):
        while True:
            recMsg = connectionSocket.recv(1024).decode("utf-8") + '\n'
            revTime = '客户端' + time.strftime('%Y-%m-%d %H:%M:%S', time.localtime()) + '\n'
            self.textEdit.config(state='normal')
            self.textEdit.insert(tk.END, revTime, 'guest')
            self.textEdit.insert(tk.END, recMsg)
            self.textEdit.see(tk.END)
            self.textEdit.config(state='disabled')

    def textSendReturn(self, event):
        if event.keysym == "Return":
            self.textSend()


root = tk.Tk()
root.title('服务端')
serverPort = 12000
serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serverSocket.bind(('', serverPort))
serverSocket.listen(1)
print('等待连接....')
connectionSocket, addr = serverSocket.accept()
print('一个连接')
app = Application(master=root)
app.mainloop()
