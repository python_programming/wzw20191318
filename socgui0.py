import socket
import tkinter as tk
import tkinter.scrolledtext as tst
import time
import tkinter.messagebox
import threading


class inputIPdialog(tk.Frame):

    def __init__(self, master):
        tk.Frame.__init__(self, master)
        self.ipInput = tk.Text(self, width=30, height=5)
        self.ipInput.grid(row=0, column=0, columnspan=3, padx=20, pady=5)
        self.okbtn = tk.Button(self, text='确定', command=self.setIP).grid(row=1, column=3)
        self.grid()

    def setIP(self):
        global servername
        servername = self.ipInput.get('1.0', 'end-1c')
        ipRootFrame.destroy()


class Application(tk.Frame):
    def __init__(self, master):
        tk.Frame.__init__(self, master)
        self.grid()
        self.createWidgets()

    def createWidgets(self):
        self.textEdit = tst.ScrolledText(self, width=50, height=15)
        self.textEdit.grid(row=0, column=0, rowspan=1, columnspan=4,padx=20, pady=5)
        self.textEdit.config(state='disabled')
        self.textEdit.tag_config('server', foreground='green')
        self.textEdit.tag_config('guest', foreground='blue')
        self.inputText = tk.Text(self, width=40, height=5)
        self.inputText.grid(row=1, column=0, columnspan=1, padx=20, pady=5)
        self.inputText.bind("<KeyPress-Return>", self.textSendReturn)
        self.btnSend = tk.Button(self, text='send', command=self.textSend)
        self.btnSend.grid(row=1, column=3)
        t = threading.Thread(target=self.getInfo)
        t.start()

    def textSend(self):
        str = self.inputText.get('1.0', 'end-1c')
        if str != "" and str != None:
            timemsg = '客户端' + time.strftime('%Y-%m-%d %H:%M:%S', time.localtime()) + '\n'
            self.textEdit.config(state='normal')
            self.textEdit.insert(tk.INSERT, timemsg, 'guest')
            self.textEdit.insert(tk.INSERT, str + '\n')
            self.textEdit.see(tk.END)
            self.textEdit.config(state='disabled')
            self.inputText.delete(0.0, tk.END)
            sendMessage = bytes(str, encoding='utf8')
            clientSocket.send(sendMessage)
        else:
            tk.messagebox.showinfo('警告', "不能发送空白信息！")

    def getInfo(self):
        global clientSocket
        while True:
            recMessage = clientSocket.recv(1024).decode("utf8") + '\n'
            recTime = '服务端' + time.strftime('%Y-%m-%d %H:%M:%S', time.localtime()) + '\n'
            self.textEdit.config(state='normal')
            self.textEdit.insert(tk.END, recTime, 'server')
            self.textEdit.insert(tk.END, recMessage)
            self.textEdit.see(tk.END)
            self.textEdit.config(state='disabled')

    def textSendReturn(self, event):
        if event.keysym == "Return":
            self.textSend()


servername = ''
serverport = 12000
ipRootFrame = tk.Tk()
ipRootFrame.title('输入服务器ip')
ipDialog = inputIPdialog(ipRootFrame)
ipDialog.mainloop()
clientSocket = None

try:
    clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
except:
    tk.messagebox.showinfo('未知错误', '检查服务器地址是否错误！')
clientSocket.connect((servername, serverport))
root = tk.Tk()
root.title('客户端')

app = Application(master=root)
app.mainloop()
