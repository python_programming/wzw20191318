"""
    作者：王泽文
    文件名：socket实践
    时间：2020年5月6日
"""
import socket

while True:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('127.0.0.1', 800))
    s.listen()
    conn, address = s.accept()
    data = conn.recv(1024)
    if data.decode() == "对方已退出":
        print(data.decode())
        s.close()
        break
    else:
        print(data.decode())
        conn.sendall(("已接收：" + str(data.decode())).encode())