"""
    作者：王泽文
    文件名：序列实践2
    时间：2020年3月18日
"""
animates = [("异度侵入", 9.9),
            ("因为太怕痛就全点防御力了", 8.9),
            ("入间同学入魔了", 9.5),
            ("Re：从零开始第异世界生活新编集版", 9.6),
            ("命运-冠位指定 绝对魔兽战线巴比伦尼亚", 9.4),
            ("虚构推理", 9.3),
            ("黑色四叶草", 9.4),
            ("某科学的超电磁炮T", 9.8),
            ("碧蓝航线", 8.4),
            ("地缚少年花子君", 9.6)]

while True:
    animates = sorted(animates, key=lambda s: s[1], reverse=True)
    print("bilibili热门番剧排行榜：")
    l0 = len(animates)
    for i in range(l0):
        print('番名：%s     评分：%.1f' % (animates[i][0], animates[i][1]))
    print("增加/修改/删除/退出")
    a = input("请输入：")
    if a == "增加":
        name = input("请输入番剧名：")
        num = float(input("请输入番剧评分："))
        animate = (name, num)
        animates.append(animate)
        print("添加成功\n")
        continue
    elif a == "删除":
        name = input("请输入番剧名：")
        for i in range(l0):
            if name == animates[i][0]:
                q = i
        del animates[q]
        print("删除成功\n")
        continue
    elif a == "修改":
        name = input("请输入番剧名：")
        num = float(input("请输入番剧评分："))
        animate = (name, num)
        t = 0
        for i in range(l0):
            if name == animates[i][0]:
                q = i
                t = 1
        del animates[q]
        animates.append(animate)
        if t == 1:
            print("修改成功\n")
        else:
            print("该番剧不在该列表中")
        continue
    elif a == "退出":
        break
    else:
        print("输入错误")
        continue
