"""
    作者：王泽文
    文件名：
    时间：2020年5月27日19:59:50
"""
import re

import requests
import json
import xmltodict
import time
import bs4


def find_danmu():
    barrages_cs = []
    aid = input("input av:")
    headers = {"user-agent": "Mozilla/5.0"}
    url = f"https://api.bilibili.com/x/player/pagelist?aid={aid}&jsonp=jsonp"
    response = requests.get(url, headers=headers).text
    cid_dict_list = json.loads(response)["data"]
    # print(cid_dict_list)
    # print(len(cid_dict_list))
    for cid in cid_dict_list:
        cid = cid["cid"]
        url = f"https://api.bilibili.com/x/v1/dm/list.so?oid={cid}"
        print(url)
        barrages_xml = requests.get(url, headers=headers).content.decode("utf-8")
        barrages_json = xmltodict.parse(barrages_xml)
        barrages_str = json.dumps(barrages_json)
        barrages = json.loads(barrages_str).get("i").get("d")
        for barrage in barrages:
            if "#text" in barrage:
                barrage = barrage["#text"] + ","
                print(barrage)
                barrages_cs.append(barrage)
                barrages_list = open("av" + aid + "的弹幕.csv", "a", newline="\n", encoding="utf-8")
                barrages_list.write(barrage)
                b = open("av" + aid + "的弹幕.txt", "a", newline="\n", encoding="utf-8")
                b.write(barrage + "\n")
    print(len(barrages_cs))


def find_pinglun(page):
    commentlist = []
    hlist = []
    hlist.append("UUID")
    hlist.append("名字")
    hlist.append("性别")
    hlist.append("时间")
    hlist.append("评论")
    hlist.append("点赞数")
    hlist.append("回复数")
    commentlist.append(hlist)
    aid = input("input av:")
    for n in range(20):
        page = str(n+1)
        headers = {"user-agent": "Mozilla/5.0"}
        url = f"http://api.bilibili.com/x/v2/reply?jsonp=jsonp&;pn={page}&type=1&oid={aid}"
        try:
            response = requests.get(url, headers=headers).text
            barrages = json.loads(response)
            for i in range(20):
                comment = barrages['data']['replies'][i]
                blist = []
                mid = comment['member']['mid']
                username = comment['member']['uname']
                sex = comment['member']['sex']
                ctime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(comment['ctime']))
                content = comment['content']['message']
                likes = comment['like']
                rcounts = comment['rcount']
                signature = comment['member']['sign']
                level = comment['member']['level_info']['current_level']
                blist.append(mid)
                blist.append(username)
                blist.append(sex)
                blist.append(ctime)
                blist.append(content)
                blist.append(likes)
                blist.append(rcounts)
                blist.append(signature)
                blist.append(level)
                commentlist.append(blist)
        except requests.HTTPError as e:
            print(e)
        except requests.RequestException as e:
            print(e)
        except IndexError:
            print("IndexError")
        except:
            print("Unknown Error!")

    for i in commentlist:
        print(i)


def upfans():
    uid = input("upid:")
    fans_list = []
    while True:
        url = f'https://api.bilibili.com/x/web-interface/card?mid={uid}&;jsonp=jsonp&article=true'
        url_2 = f'https://api.bilibili.com/x/relation/followers?vmid={uid}&pn=1&ps=20&order=desc'
        headers = {"user-agent": "Mozilla/5.0"}
        fans_id = requests.get(url_2, headers=headers).text
        reply = requests.get(url, headers=headers).text
        fans_name = json.loads(fans_id).get("data").get("list")
        fans = json.loads(reply).get("data").get("card").get("fans")
        up_id = json.loads(reply).get("data").get("card").get("name")
        print("up_id:", up_id, "fans:", fans, "\n")
        for name in fans_name:
            flag = 0
            name = name["uname"]
            for fan in fans_list:
                if str(name) == fan:
                    flag = 1
            if flag == 0:
                fans_list.append(name)
                print("新粉：", name)
        a = time.strftime('%Y-%m-%d %H:%M:%S秒')
        print(a)
        print("_____________________________________")
        time.sleep(5)


def main_message():
    aid = input("input av:")
    url = f'http://api.bilibili.com/archive_stat/stat?aid={aid}&type=jsonp'
    url2 = f'https://api.bilibili.com/x/web-interface/archive/desc?&aid={aid}'
    url3 = f'https://www.bilibili.com/video/av{aid}'
    headers = {"user-agent": "Mozilla/5.0"}
    try:
        response1 = requests.get(url, headers=headers).text
        response2 = requests.get(url2, headers=headers).text
        response3 = requests.get(url3, headers=headers).text
        barrages = json.loads(response1)
        barrages_2 = json.loads(response2)
    except requests.HTTPError as e:
        print(e)
    except requests.RequestException as e:
        print(e)
    except:
        print("Unknown Error!")
    text = r'<title data-vue-meta="true">(.*?)</title>'
    text0 =r'<meta data-vue-meta="true" itemprop="author" name="author" content="(.*?)">'
    text1 = re.findall(text, response3)
    text2 = re.findall(text0, response3)
    print(" 标题："+text1[0]+"\n"+" up："+text2[0])
    print(" av号：av" + str(aid) + "\n", "观看：" + str(barrages['data']['view']) + "  弹幕：" + str(barrages['data']['danmaku']) + "  评论：" +
          str(barrages['data']['reply']) + "  点赞：" + str(barrages['data']['like']) + "  投币：" + str(barrages['data']['coin']) + "  收藏：" +
          str(barrages['data']['favorite']) + "  分享：" + str(barrages['data']['share']), )
    print(" 视频简介："+barrages_2['data'])



def up_message():
    uid = input("input up uid:")
    url = f'https://api.bilibili.com/x/relation/stat?vmid={uid}&jsonp=jsonp'
    url2 = f'https://api.bilibili.com/x/space/upstat?mid={uid}'
    url3 = f'https://space.bilibili.com/{uid}'
    text0 = r'<title>(.*?)的个人空间 - 哔哩哔哩 [(] ゜- ゜[)]つロ 乾杯~ Bilibili</title>'
    text1 = r'<meta name="description" content="(.*?)bilibili是国内知名的视频弹幕网站，这里有最及时的动漫新番，最棒的ACG氛围，最有创意的Up主。大家可以在这里找到许多欢乐。"/>'
    headers = {"user-agent": "Mozilla/5.0"}
    response = requests.get(url, headers=headers).text
    response2 = requests.get(url2, headers=headers).text
    response3 = requests.get(url3, headers=headers).text
    up_message_barrages1 = json.loads(response).get("data")
    up_message_barrages2 = json.loads(response2)
    up_uid = str(up_message_barrages1["mid"])
    up_following = str(up_message_barrages1["following"])
    up_watchv = str(up_message_barrages2['data']['archive']['view'])
    up_watcha = str(up_message_barrages2["data"]["article"]["view"])
    up_likes = str(up_message_barrages2["data"]["likes"])
    up_name = re.findall(text0, response3)
    up_main = re.findall(text1, response3)
    print("up:"+up_name[0]+"\nuid:"+up_uid+"\n关注数："+up_following+"  视频总播放量："+up_watchv+"  文章总观看量："+up_watcha+"  总点赞数："+up_likes+"\n简介："+up_main[0])

def find_up_video():
    videolist = []
    hlist = []
    hlist.append("av号")
    hlist.append("视频标题")
    hlist.append("作者")
    hlist.append("作者UUID")
    hlist.append("发布时间")
    hlist.append("视频长度")
    hlist.append("收藏数")
    hlist.append("评论数")
    hlist.append("弹幕数")
    hlist.append("播放量")
    hlist.append("视频简介")
    videolist.append(hlist)
    uid = input("input up_uid:")
    for n in range(20):
        page = str(n+1)
        headers = {"user-agent": "Mozilla/5.0"}
        url = 'http://space.bilibili.com/ajax/member/getSubmitVideos?mid='+uid+'&pagesize=20&page='+page+'&jsonp=jsonp'
        response = requests.get(url, headers=headers).text
        barrages = json.loads(response)
        v_message = barrages['data']['vlist']
        for i in v_message:
            blist = []
            aid = i['aid']
            v_tiltle = i['title']
            v_author = i['author']
            mid = i['mid']
            v_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(i['created']))
            v_length = i['length']
            v_favorites = i['favorites']
            v_comment = i['comment']
            video_review = i['video_review']
            v_play = i['play']
            v_description = i['description']
            blist.append(aid)
            blist.append(v_tiltle)
            blist.append(v_author)
            blist.append(mid)
            blist.append(v_time)
            blist.append(v_length)
            blist.append(v_favorites)
            blist.append(v_comment)
            blist.append(video_review)
            blist.append(v_play)
            blist.append(v_description)
            videolist.append(blist)
        # except requests.HTTPError as e:
        #     print(e)
        # except requests.RequestException as e:
        #     print(e)
        # except IndexError:
        #     print("IndexError")
        # except:
        #     print("Unknown Error!")

    for i in videolist:
        print(i)




if __name__ == '__main__':
    # find_pinglun(1)
    # upfans()
    # main_message()
    # up_message()
    find_up_video()