"""
    作者：王泽文
    文件名：序列的应用实践1
    时间：2020年3月18日
"""
kiana = ["领域装白练", "女武神游侠", "圣女祈祷", "白骑士月光", "空之律者", "天穹游侠"]
raiden = ["脉冲装绯红", "影舞冲击", "女武神强袭", "雷电女王的鬼铠", "破晓强袭"]
bronya = ["女武神战车", "驱动装山吹", "雪地狙击", "次元边界突破", "银狼的黎明", "异度黑核侵蚀", "彗星驱动", "理之律者"]
himeko = ["战场疾风", "融合装深红", "女武神凯旋", "血色玫瑰", "极地战刃", "真红骑士月蚀"]
sakura = ["逆神巫女", "真炎辛魂", "御神装勿忘", "夜影重霞"]
theresa = ["女武神誓约", "处刑装紫苑", "樱火轮舞", "神恩颂歌", "月下初拥", "暮光骑士月煌"]
kallen = ["圣仪装今样", "第六幻想夜", "原罪猎人"]
fuhua = ["云墨丹心", "女武神迅羽", "影骑士月轮", "炽翎", "白夜执事", "雾都迅羽"]
rita = ["黯蔷薇", "苍骑士月魂", "猎袭装影铁"]
seele = ["幻海梦蝶", "彼岸双生"]
olenyeva = ["蓝莓特工", "樱桃炸弹"]
durandal = ["女武神荣光"]
while True:
    print("崩坏3角色列表\n查询/增加/修改/删除/退出")
    a = input("请输入：")
    if a == "查询":
        print("琪亚娜卡斯兰娜：", kiana,
              "雷电芽衣：", raiden,
              "布洛妮娅扎伊切克", bronya,
              "无量塔姬子", himeko,
              "八重樱", sakura,
              "德丽莎阿波卡利斯", theresa,
              "卡莲卡斯兰娜", kallen,
              "符华", fuhua,
              "丽塔", rita,
              "希儿", seele,
              "阿琳姐妹",
              olenyeva,
              "幽兰黛尔", durandal,
              sep='\n')
        continue
    if a == "增加" or a == "删除" or a == "修改":
        print("角色种类：\n琪亚娜卡斯兰娜/雷电芽衣/布洛妮娅扎伊切克/无量塔姬子/八重樱/德丽莎阿波卡利斯/卡莲卡斯兰娜/符华/丽塔/希儿/阿琳姐妹/幽兰黛尔")
        b = input("请输入角色种类：")
        c = input("请输入角色名称：")
        # 琪亚娜卡斯兰娜
        if b == "琪亚娜卡斯兰娜":
            print(kiana)
            if a == "增加":
                if c in kiana:
                    print("该角色已存在")
                else:
                    kiana.append(c)
                    print("添加成功\n", kiana)
            if a == "删除":
                if c in kiana:
                    kiana.remove(c)
                    print("删除成功\n", kiana)
                else:
                    print("该角色不存在")
            if a == "修改":
                if c in kiana:
                    d = kiana.index(c)
                    kiana[d] = input("替换角色名称为")
                    print("修改成功\n", kiana)
                else:
                    print("该角色不存在")
            continue
        # 雷电芽衣
        if b == "雷电芽衣":
            print(raiden)
            if a == "增加":
                if c in raiden:
                    print("该角色已存在")
                else:
                    raiden.append(c)
                    print("添加成功\n", raiden)
            if a == "删除":
                if c in raiden:
                    raiden.remove(c)
                    print("删除成功\n", raiden)
                else:
                    print("该角色不存在")
            if a == "修改":
                if c in raiden:
                    d = raiden.index(c)
                    raiden[d] = input("替换角色名称为")
                    print("修改成功\n", raiden)
                else:
                    print("该角色不存在")
            continue
        # 布洛妮娅扎伊切克
        if b == "布洛妮娅扎伊切克":
            print(bronya)
            if a == "增加":
                if c in bronya:
                    print("该角色已存在")
                else:
                    bronya.append(c)
                    print("添加成功\n", bronya)
            if a == "删除":
                if c in bronya:
                    bronya.remove(c)
                    print("删除成功\n", bronya)
                else:
                    print("该角色不存在")
            if a == "修改":
                if c in bronya:
                    d = bronya.index(c)
                    bronya[d] = input("替换角色名称为")
                    print("修改成功\n", bronya)
                else:
                    print("该角色不存在")
            continue
        # 无量塔姬子
        if b == "无量塔姬子":
            print(himeko)
            if a == "增加":
                if c in himeko:
                    print("该角色已存在")
                else:
                    himeko.append(c)
                    print("添加成功\n", himeko)
            if a == "删除":
                if c in himeko:
                    himeko.remove(c)
                    print("删除成功\n", himeko)
                else:
                    print("该角色不存在")
            if a == "修改":
                if c in himeko:
                    d = himeko.index(c)
                    himeko[d] = input("替换角色名称为")
                    print("修改成功\n", himeko)
                else:
                    print("该角色不存在")
            continue
        # 八重樱
        if b == "八重樱":
            print(sakura)
            if a == "增加":
                if c in sakura:
                    print("该角色已存在")
                else:
                    sakura.append(c)
                    print("添加成功\n", sakura)
            if a == "删除":
                if c in sakura:
                    sakura.remove(c)
                    print("删除成功\n", sakura)
                else:
                    print("该角色不存在")
            if a == "修改":
                if c in sakura:
                    d = sakura.index(c)
                    sakura[d] = input("替换角色名称为")
                    print("修改成功\n", sakura)
                else:
                    print("该角色不存在")
            continue
        else:
            print("该角色此功能未开放")
            continue
    if a == "退出":
        print("谢谢使用")
        break
