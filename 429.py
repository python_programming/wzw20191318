"""
    作者：王泽文
    文件名：数据库实践
    时间：2020年4月29日
"""
import sqlite3

conn = sqlite3.connect("1318.db")
cursor = conn.cursor()
cursor.execute('create table if not exists student (number int(10) primary key, age int(10), name varchar(20), '
               'score float(20) )')
cursor.execute('insert into student (number, age, name, score) values(1601, 19, "张三", 80)')
cursor.execute('insert into student (number, age, name, score) values(1602, 18, "李四", 59)')
cursor.execute('insert into student (number, age, name, score) values(1603, 19, "王五", 65)')
cursor.execute('insert into student (number, age, name, score) values(1604, 20, "赵六", 77)')
cursor.execute('select * from student')
print(cursor.fetchall())
cursor.execute('update student set score = 88 where number = 1604')
cursor.execute('select * from student')
print(cursor.fetchall())
cursor.execute('delete from student where number = 1603')
cursor.execute('select * from student')
print(cursor.fetchall())
conn.close()