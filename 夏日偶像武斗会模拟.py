"""
    名称：崩坏三女武神决斗模拟
"""
import random

base_data = [("琪亚娜", 100, 11, 24, 23, 100, 1, 0, 0, 1), ("芽衣", 100, 12, 22, 30, 100, 1, 0, 0, 1),
             ("布洛妮娅", 100, 10, 21, 20, 100, 1, 0, 0, 1), ("姬子", 100, 9, 23, 12, 100, 1, 0, 0, 1),
             ("丽塔", 100, 11, 26, 17, 100, 1, 0, 0, 1),
             ("八重樱&卡莲", 100, 9, 20, 18, 100, 2, 0, 0, 1), ("渡鸦", 100, 14, 23, 14, 100, 1, 0, 0, 1),
             ("德丽莎", 100, 12, 19, 22, 100, 1, 0, 0, 1),
             ("罗莎莉亚&莉莉娅", 100, 10, 18, 10, 100, 2, 0, 0, 1, 0),
             ("希儿", 100, 13, 23, 26, 100, 1, 0, 0, 1, 0), ("幽兰黛尔&屎蛋", 100, 10, 19, 15, 100, 1, 0, 0, 1),
             ("符华", 100, 15, 17, 16, 100, 1, 0, 0, 1)]  # (名称，生命，防御，攻击，速度，命中，人数，自身可使用回合,状态回合,伤害倍率，阿琳姐妹死亡次数/希儿状态)


def qyn(me, you, all_h):
    if me[7] > 0:
        me[7] = me[7] - 1
        if me[8] > 0:
            print(me[0], "处于魅惑状态")
            print("琪亚娜使用普通攻击")
            sh = (me[3] - you[2]) * me[9]
            m1 = random.randint(0, 99)
            if m1 < me[5]:
                if sh > 0:
                    you[1] = you[1] - sh
                    print("对", you[0], "造成", sh, "点伤害")
                else:
                    print("对", you[0], "造成0点伤害")
            else:
                print("miss")
            me[8] = me[8] - 1
        else:
            if all_h % 2 == 0:
                print("琪亚娜使用 吃我一猫 ，用亚空之猫直接攻击对方血条")
                m3 = random.randint(0, 99)
                if you[0] == "幽兰黛尔&屎蛋" and you[8] == 0 and m3 < 16:
                    print("幽兰黛尔使用 反弹！反弹无效！")
                    me[1] = me[1] - (30 - me[2])
                else:
                    sh = (me[3] + you[2]) * me[9]
                    m1 = random.randint(0, 99)
                    if m1 < me[5]:
                        if sh > 0:
                            you[1] = you[1] - sh
                            print("对", you[0], "造成", sh, "点伤害")
                        else:
                            print("对", you[0], "造成0点伤害")
                    else:
                        print("miss")
                m2 = random.randint(0, 99)
                if m2 < 35:
                    me[7] = me[7] - 1
                    print("琪亚娜使用 音浪~太强~\n琪亚娜虫化并眩晕一回合")
            else:
                print("琪亚娜使用普通攻击")
                sh = (me[3] - you[2]) * me[9]
                m1 = random.randint(0, 99)
                if m1 < me[5]:
                    if sh > 0:
                        you[1] = you[1] - sh
                        print("对", you[0], "造成", sh, "点伤害")
                    else:
                        print("对", you[0], "造成0点伤害")
                else:
                    print("miss")


def yy(me, you, all_h):
    if me[7] > 0:
        me[7] = me[7] - 1
        if me[8] > 0:
            print(me[0], "处于魅惑状态")
            sh = (me[3] - you[2]) * me[9]
            m1 = random.randint(0, 99)
            if m1 < me[5]:
                if sh > 0:
                    you[1] = you[1] - sh
                    print("对", you[0], "造成", sh, "点伤害")
                else:
                    print("对", you[0], "造成0点伤害")
            else:
                print("miss")
            me[8] = me[8] - 1
        else:
            if all_h % 2 == 0:
                print("芽衣使用 雷电家的龙女仆")
                m3 = random.randint(0, 99)
                if you[0] == "幽兰黛尔&屎蛋" and you[8] == 0 and m3 < 16:
                    print("幽兰黛尔使用 反弹！反弹无效！")
                    me[1] = me[1] - (30 - me[2])
                else:
                    sh = 15
                    m1 = (random.randint(0, 99)) * me[9]
                    if m1 < me[5]:
                        if sh > 0:
                            you[1] = you[1] - sh
                            print("对", you[0], "造成", sh, "点无视防御的元素伤害")
                        else:
                            print("对", you[0], "造成0点伤害")
                    else:
                        print("miss")
            else:
                print("芽衣使用普通攻击")
                sh = (me[3] - you[2]) * me[9]
                m3 = random.randint(0, 99)
                if m3 < me[5]:
                    if sh > 0:
                        you[1] = you[1] - sh
                        print("对", you[0], "造成", sh, "点伤害")
                    else:
                        print("对", you[0], "造成0点伤害")
                else:
                    print("miss")
            m2 = random.randint(0, 99)
            if m2 < 30:
                you[7] = you[7] - 1
                print("芽衣使用 崩坏世界的歌姬\n麻痹了对方一回合")


def blny(me, you, all_h):
    if me[7] > 0:
        me[7] = me[7] - 1
        if me[8] > 0:
            print(me[0], "处于魅惑状态")
            print("布洛妮娅使用普通攻击")
            sh = (me[3] - you[2]) * me[9]
            m1 = random.randint(0, 99)
            if m1 < me[5]:
                if sh > 0:
                    you[1] = you[1] - sh
                    print("对", you[0], "造成", sh, "点伤害")
                else:
                    print("对", you[0], "造成0点伤害")
            else:
                print("miss")
            me[8] = me[8] - 1
        else:
            if all_h % 3 == 0:
                print("布洛妮娅使用 摩托拜客哒！用摩托车碾压对手")
                m3 = random.randint(0, 99)
                if you[0] == "幽兰黛尔&屎蛋" and you[8] == 0 and m3 < 16:
                    print("幽兰黛尔使用 反弹！反弹无效！")
                    me[1] = me[1] - (30 - me[2])
                else:
                    sh = (random.randint(1, 100)) * me[9]
                    m1 = random.randint(0, 99)
                    if m1 < me[5]:
                        if sh > 0:
                            you[1] = you[1] - sh
                            print("对", you[0], "造成", sh, "点无视防御的元素伤害")
                    else:
                        print("miss")
            else:
                print("布洛妮娅使用普通攻击")
                sh = (me[3] - you[2]) * me[9]
                m3 = random.randint(0, 99)
                if m3 < me[5]:
                    if sh > 0:
                        you[1] = you[1] - sh
                        print("对", you[0], "造成", sh, "点伤害")
                    else:
                        print("对", you[0], "造成0点伤害")
                else:
                    print("miss")
            m2 = random.randint(0, 99)
            if m2 < 25:
                sh = 4 * (12 - you[2]) * me[9]
                if sh > 0:
                    you[1] = you[1] - sh
                    print("布洛妮娅使用 天使重构 对", you[0], "造成", sh, "点伤害")
                else:
                    print("布洛妮娅使用 天使重构 对", you[0], "造成0点伤害")


def jz(me, you, all_h):
    if me[7] > 0:
        me[7] = me[7] - 1
        if me[8] > 0:
            print(me[0], "处于魅惑状态")
            print("姬子使用普通攻击")
            sh = (me[3] - you[2]) * me[9]
            m1 = random.randint(0, 99)
            if m1 < me[5]:
                if sh > 0:
                    you[1] = you[1] - sh
                    print("对", you[0], "造成", sh, "点伤害")
                else:
                    print("对", you[0], "造成0点伤害")
            else:
                print("miss")
            me[8] = me[8] - 1
        else:
            bl = 1
            if you[6] > 1:
                print("姬子使用 真爱不死\n姬子的伤害提高了。。。啊啊啊啊")
                bl = 2
            if all_h % 2 == 0:
                print("姬子使用 干杯，朋友\n姬子的攻击提高了，但命中下降了")
                m3 = random.randint(0, 99)
                if you[0] == "幽兰黛尔&屎蛋" and you[8] == 0 and m3 < 16:
                    print("幽兰黛尔使用 反弹！反弹无效！")
                    me[1] = me[1] - (30 - me[2])
                else:
                    me[3] = me[3] * 2
                    sh = (me[3] - you[2]) * me[9] * bl
                    me[5] = me[5] * 0.65
                    m1 = random.randint(0, 99)
                    if m1 < me[5]:
                        if sh > 0:
                            you[1] = you[1] - sh
                            print("对", you[0], "造成", sh, "点伤害")
                        else:
                            print("对", you[0], "造成0点伤害")
                    else:
                        print("miss")
            else:
                print("姬子使用普通攻击")
                sh = (me[3] - you[2]) * me[9] * bl
                m3 = random.randint(0, 99)
                if m3 < me[5]:
                    if sh > 0:
                        you[1] = you[1] - sh
                        print("对", you[0], "造成", sh, "点伤害")
                    else:
                        print("对", you[0], "造成0点伤害")
                else:
                    print("miss")


def lt(me, you, all_h):
    if me[7] > 0:
        me[7] = me[7] - 1
        if me[8] > 0:
            print(me[0], "处于魅惑状态")
            print("丽塔使用普通攻击")
            sh = (me[3] - you[2]) * me[9]
            m1 = random.randint(0, 99)
            if m1 < me[5]:
                if sh > 0:
                    you[1] = you[1] - sh
                    print("对", you[0], "造成", sh, "点伤害")
                else:
                    print("对", you[0], "造成0点伤害")
            else:
                print("miss")
            me[8] = me[8] - 1
        else:
            if all_h % 4 == 0:
                print("丽塔使用 完美心意\n对方的血量提升了。。。（大雾），对方的伤害永久下降了")
                m3 = random.randint(0, 99)
                if you[0] == "幽兰黛尔&屎蛋" and you[8] == 0 and m3 < 16:
                    print("幽兰黛尔使用 反弹！反弹无效！")
                    me[1] = me[1] - (30 - me[2])
                else:
                    you[1] = you[1] + 4
                    if you[1] > 100:
                        you[1] = 100
                    you[8] = you[8] + 2
                    you[9] = 0.4
                    print(you[0], "进入魅惑状态")
            else:
                print("丽塔使用普通攻击")
                m2 = random.randint(0, 99)
                sh = (me[3] - you[2]) * me[9]
                m3 = random.randint(0, 99)
                if m3 < me[5]:
                    if m2 < 35:
                        sh = sh - 3
                        you[3] = you[3] - 4
                        print("丽塔使用 女仆的温柔清理\n丽塔的伤害下降了，对方的攻击下降了")
                        if sh > 0:
                            you[1] = you[1] - sh
                            print("对", you[0], "造成", sh, "点伤害")
                        else:
                            print("对", you[0], "造成0点伤害")
                    else:
                        if sh > 0:
                            you[1] = you[1] - sh
                            print("对", you[0], "造成", sh, "点伤害")
                        else:
                            print("对", you[0], "造成0点伤害")
                else:
                    print("miss")


def bcykl(me, you, all_h):
    if me[7] > 0:
        me[7] = me[7] - 1
        if me[8] > 0:
            print(me[0], "处于魅惑状态")
            print("樱莲组使用普通攻击")
            sh = (me[3] - you[2]) * me[9]
            m1 = random.randint(0, 99)
            if m1 < me[5]:
                if sh > 0:
                    you[1] = you[1] - sh
                    print("对", you[0], "造成", sh, "点伤害")
                else:
                    print("对", you[0], "造成0点伤害")
            else:
                print("miss")
            me[8] = me[8] - 1
        else:
            m0 = random.randint(0, 99)
            if m0 < 30:
                print("樱莲组使用 八重樱的饭团\n自身的血量回复了")
                me[1] = me[1] + 25
                if me[1] > 100:
                    me[1] = 100
            if all_h % 2 == 0:
                print("樱莲组使用 卡莲的饭团  \n发射了一个超大饭团")
                m3 = random.randint(0, 99)
                if you[0] == "幽兰黛尔&屎蛋" and you[8] == 0 and m3 < 16:
                    print("幽兰黛尔使用 反弹！反弹无效！")
                    me[1] = me[1] - (30 - me[2])
                else:
                    sh = 25 * me[9]
                    m1 = random.randint(0, 99)
                    if m1 < me[5]:
                        if sh > 0:
                            you[1] = you[1] - sh
                            print("对", you[0], "造成", sh, "点伤害")
                        else:
                            print("对", you[0], "造成0点伤害")
                    else:
                        print("miss")
            else:
                print("樱莲组使用普通攻击")
                sh = (me[3] - you[2]) * me[9]
                m3 = random.randint(0, 99)
                if m3 < me[5]:
                    if sh > 0:
                        you[1] = you[1] - sh
                        print("对", you[0], "造成", sh, "点伤害")
                    else:
                        print("对", you[0], "造成0点伤害")
                else:
                    print("miss")


def dy_start(l, r):
    if l[0] == "渡鸦" and r[0] == "琪亚娜":
        print("渡鸦使用 不是针对你\n渡鸦的伤害提高了")
        l[9] = 1.25
    elif r[0] == "渡鸦" and l[0] == "琪亚娜":
        print("渡鸦使用 不是针对你\n渡鸦的伤害提高了")
        r[9] = 1.25
    elif l[0] == "渡鸦" and r[0] != "琪亚娜":
        m3 = random.randint(0, 99)
        if m3 < 25:
            print("渡鸦使用 不是针对你\n渡鸦的伤害提高了")
            l[9] = 1.25
    elif r[0] == "渡鸦" and l[0] != "琪亚娜":
        m3 = random.randint(0, 99)
        if m3 < 25:
            print("渡鸦使用 不是针对你\n渡鸦的伤害提高了")
            r[9] = 1.25


def dy(me, you, all_h):
    if me[7] > 0:
        me[7] = me[7] - 1
        if me[8] > 0:
            print(me[0], "处于魅惑状态")
            print("渡鸦使用普通攻击")
            sh = (me[3] - you[2]) * me[9]
            m1 = random.randint(0, 99)
            if m1 < me[5]:
                if sh > 0:
                    you[1] = you[1] - sh
                    print("对", you[0], "造成", sh, "点伤害")
                else:
                    print("对", you[0], "造成0点伤害")
            else:
                print("miss")
            me[8] = me[8] - 1
        else:
            if all_h % 3 == 0:
                print("渡鸦使用 别墅小岛  \n渡鸦大哭：小岛，我的别墅小岛，呜啊啊啊啊啊啊")
                m3 = random.randint(0, 99)
                if you[0] == "幽兰黛尔&屎蛋" and you[8] == 0 and m3 < 16:
                    print("幽兰黛尔使用 反弹！反弹无效！")
                    me[1] = me[1] - (30 - me[2])
                else:
                    sh = 7 * (16 - you[2]) * me[9]
                    m1 = random.randint(0, 99)
                    if m1 < me[5]:
                        if sh > 0:
                            you[1] = you[1] - sh
                            print("对", you[0], "造成", sh, "点伤害")
                        else:
                            print("对", you[0], "造成0点伤害")
                    else:
                        print("miss")
            else:
                print("渡鸦使用普通攻击")
                sh = (me[3] - you[2]) * me[9]
                m3 = random.randint(0, 99)
                if m3 < me[5]:
                    if sh > 0:
                        you[1] = you[1] - sh
                        print("对", you[0], "造成", sh, "点伤害")
                    else:
                        print("对", you[0], "造成0点伤害")
                else:
                    print("miss")


def dls(me, you, all_h):
    if me[7] > 0:
        me[7] = me[7] - 1
        if me[8] > 0:
            print(me[0], "处于魅惑状态")
            print("德丽莎使用普通攻击")
            sh = (me[3] - you[2]) * me[9]
            m1 = random.randint(0, 99)
            if m1 < me[5]:
                if sh > 0:
                    you[1] = you[1] - sh
                    print("对", you[0], "造成", sh, "点伤害")
                else:
                    print("对", you[0], "造成0点伤害")
            else:
                print("miss")
            me[8] = me[8] - 1
        else:
            if all_h % 3 == 0:
                print("德丽莎使用 在线踢人")
                m3 = random.randint(0, 99)
                if you[0] == "幽兰黛尔&屎蛋" and you[8] == 0 and m3 < 16:
                    print("幽兰黛尔使用 反弹！反弹无效！")
                    me[1] = me[1] - (30 - me[2])
                else:
                    sh = 5 * (16 - you[2]) * me[9]
                    m1 = random.randint(0, 99)
                    if m1 < me[5]:
                        if sh > 0:
                            you[1] = you[1] - sh
                            print("对", you[0], "造成", sh, "点伤害")
                        else:
                            print("对", you[0], "造成0点伤害")
                    else:
                        print("miss")
            else:
                print("德丽莎使用普通攻击")
                sh = (me[3] - you[2]) * me[9]
                m3 = random.randint(0, 99)
                if m3 < me[5]:
                    if sh > 0:
                        you[1] = you[1] - sh
                        print("对", you[0], "造成", sh, "点伤害")
                    else:
                        print("对", you[0], "造成0点伤害")
                else:
                    print("miss")
            m4 = random.randint(0, 99)
            if m4 < 30:
                you[2] = you[2] - 5
                print("德丽莎使用 血犹大第一可爱")


def aljm(me, you, all_h):
    if me[7] > 0:
        me[7] = me[7] - 1
        if me[8] > 0:
            print(me[0], "处于魅惑状态")
            print("阿琳姐妹使用普通攻击")
            sh = (me[3] - you[2]) * me[9]
            m1 = random.randint(0, 99)
            if m1 < me[5]:
                if sh > 0:
                    you[1] = you[1] - sh
                    print("对", you[0], "造成", sh, "点伤害")
                else:
                    print("对", you[0], "造成0点伤害")
            else:
                print("miss")
            me[8] = me[8] - 1
        else:
            if me[10] == 1:
                me[10] = 2
                print("阿琳姐妹使用 变成星星吧！")
                m3 = random.randint(0, 99)
                if you[0] == "幽兰黛尔&屎蛋" and you[8] == 0 and m3 < 16:
                    print("幽兰黛尔使用 反弹！反弹无效！")
                    me[1] = me[1] - (30 - me[2])
                else:
                    m0 = random.randint(0, 99)
                    if m0 < 50:
                        sh = (233 - you[2]) * me[9]
                    else:
                        sh = (50 - you[2]) * me[9]
                    m1 = random.randint(0, 99)
                    if m1 < me[5]:
                        if sh > 0:
                            you[1] = you[1] - sh
                            print("对", you[0], "造成", sh, "点伤害")
                        else:
                            print("对", you[0], "造成0点伤害")
                    else:
                        print("miss")
            else:
                print("阿琳姐妹使用普通攻击")
                sh = (me[3] - you[2]) * me[9]
                m3 = random.randint(0, 99)
                if m3 < me[5]:
                    if sh > 0:
                        you[1] = you[1] - sh
                        print("对", you[0], "造成", sh, "点伤害")
                    else:
                        print("对", you[0], "造成0点伤害")
                else:
                    print("miss")


def xier(me, you, all_h):
    if me[7] > 0:
        me[7] = me[7] - 1
        if me[8] > 0:
            print(me[0], "处于魅惑状态")
            print("希儿使用普通攻击")
            sh = (me[3] - you[2]) * me[9]
            m1 = random.randint(0, 99)
            if m1 < me[5]:
                if sh > 0:
                    you[1] = you[1] - sh
                    print("对", you[0], "造成", sh, "点伤害")
                else:
                    print("对", you[0], "造成0点伤害")
            else:
                print("miss")
            me[8] = me[8] - 1
        else:
            me[10] = me[10] + 1
            print("希儿使用 我换我自己")
            print("希儿使用 拜托了另一个我")
            if me[10] % 2 == 0:
                print("希儿变成白希")
                m3 = random.randint(0, 99)
                if you[0] == "幽兰黛尔&屎蛋" and you[8] == 0 and m3 < 16:
                    print("幽兰黛尔使用 反弹！反弹无效！")
                    me[1] = me[1] - (30 - me[2])
                else:
                    m0 = random.randint(1, 15)
                    me[1] = me[1] + m0
                    print("回复了", m0, "点生命")
                    me[3] = me[3] - 10
                    me[2] = me[2] + 5
            else:
                print("希儿变成黑希")
                m3 = random.randint(0, 99)
                if you[0] == "幽兰黛尔&屎蛋" and you[8] == 0 and m3 < 16:
                    print("幽兰黛尔使用 反弹！反弹无效！")
                    me[1] = me[1] - (30 - me[2])
                else:
                    me[3] = me[3] + 10
                    me[2] = me[2] - 5
            print("希儿使用普通攻击")
            sh = (me[3] - you[2]) * me[9]
            m3 = random.randint(0, 99)
            if m3 < me[5]:
                if sh > 0:
                    you[1] = you[1] - sh
                    print("对", you[0], "造成", sh, "点伤害")
                else:
                    print("对", you[0], "造成0点伤害")
            else:
                print("miss")


def ylde(me, you, all_h):
    if me[7] > 0:
        me[7] = me[7] - 1
        if me[8] > 0:
            print(me[0], "处于魅惑状态")
            print("幽兰黛尔使用普通攻击")
            sh = (me[3] - you[2]) * me[9]
            m1 = random.randint(0, 99)
            if m1 < me[5]:
                if sh > 0:
                    you[1] = you[1] - sh
                    print("对", you[0], "造成", sh, "点伤害")
                else:
                    print("对", you[0], "造成0点伤害")
            else:
                print("miss")
            me[8] = me[8] - 1
        else:
            me[3] = me[3] + 3
            print("幽兰黛尔使用 摸鱼的快乐")
            print("幽兰黛尔使用普通攻击")
            sh = (me[3] - you[2]) * me[9]
            m3 = random.randint(0, 99)
            if m3 < me[5]:
                if sh > 0:
                    you[1] = you[1] - sh
                    print("对", you[0], "造成", sh, "点伤害")
                else:
                    print("对", you[0], "造成0点伤害")
            else:
                print("miss")


def fh(me, you, all_h):
    if me[7] > 0:
        me[7] = me[7] - 1
        if me[8] > 0:
            print(me[0], "处于魅惑状态")
            print("符华使用普通攻击")
            sh = (me[3] - you[2]) * me[9]
            m1 = random.randint(0, 99)
            if m1 < me[5]:
                if sh > 0:
                    you[1] = you[1] - sh
                    print("对", you[0], "造成", sh, "点伤害")
                else:
                    print("对", you[0], "造成0点伤害")
            else:
                print("miss")
            me[8] = me[8] - 1
        else:
            print("符华使用 凭神化剑\n对方所有的防御力都消失了（似乎）")
            if all_h % 3 == 0:
                print("符华使用 形之笔墨\n符华泼了对手一脸墨水")
                m3 = random.randint(0, 99)
                if you[0] == "幽兰黛尔&屎蛋" and you[8] == 0 and m3 < 16:
                    print("幽兰黛尔使用 反弹！反弹无效！")
                    me[1] = me[1] - (30 - me[2])
                else:
                    sh = 18
                    you[5] = you[5] * 0.75
                    m1 = random.randint(0, 99)
                    if m1 < me[5]:
                        if sh > 0:
                            you[1] = you[1] - sh
                            print("对", you[0], "造成", sh, "点伤害")
                        else:
                            print("对", you[0], "造成0点伤害")
                    else:
                        print("miss")
            else:
                print("符华使用普通攻击")
                sh = me[3] * me[9]
                m3 = random.randint(0, 99)
                if m3 < me[5]:
                    if sh > 0:
                        you[1] = you[1] - sh
                        print("对", you[0], "造成", sh, "点伤害")
                    else:
                        print("对", you[0], "造成0点伤害")
                else:
                    print("miss")


def gj(me, you, all_h):
    if me[0] == "琪亚娜":
        qyn(me, you, all_h)
    if me[0] == "芽衣":
        yy(me, you, all_h)
    if me[0] == "布洛妮娅":
        blny(me, you, all_h)
    if me[0] == "姬子":
        jz(me, you, all_h)
    if me[0] == "丽塔":
        lt(me, you, all_h)
    if me[0] == "八重樱&卡莲":
        bcykl(me, you, all_h)
    if me[0] == "渡鸦":
        dy(me, you, all_h)
    if me[0] == "德丽莎":
        dls(me, you, all_h)
    if me[0] == "罗莎莉亚&莉莉娅":
        aljm(me, you, all_h)
    if me[0] == "希儿":
        xier(me, you, all_h)
    if me[0] == "幽兰黛尔&屎蛋":
        ylde(me, you, all_h)
    if me[0] == "符华":
        fh(me, you, all_h)
    if you[0] == "罗莎莉亚&莉莉娅" and you[1] <= 0:
        if you[10] == 0:
            you[10] = 1
            you[1] = 20
            print("阿琳姐妹使用 96度生命之水\n惊不惊喜意不意外，爷又活过来了")


def start(left_d, right_d):
    l = left_d
    r = right_d
    dy_start(l, r)
    all_h = 0
    while l[1] > 0 and r[1] > 0:
        all_h = all_h + 1
        print("剩余血量：", l[0], l[1], r[0], r[1])
        print("第", all_h, "回合")
        l[7] = l[7] + 1
        r[7] = r[7] + 1
        if l[4] > r[4]:
            gj(l, r, all_h)
            if r[1] <= 0:
                print(l[0], "获胜")
                return "l"
            if l[1] <= 0:
                print(r[0], "获胜")
                return "r"
            gj(r, l, all_h)
            if r[1] <= 0:
                print(l[0], "获胜")
                return "l"
            if l[1] <= 0:
                print(r[0], "获胜")
                return "r"
        elif r[4] > l[4]:
            gj(r, l, all_h)
            if r[1] <= 0:
                print(l[0], "获胜")
                return "l"
            if l[1] <= 0:
                print(r[0], "获胜")
                return "r"
            gj(l, r, all_h)
            if r[1] <= 0:
                print(l[0], "获胜")
                return "l"
            if l[1] <= 0:
                print(r[0], "获胜")
                return "r"


for i in range(0, 12):
    print(i + 1, ",", base_data[i][0], sep="")
l1 = int(input("左边的选手:"))
l = 0
ld = base_data[l1 - 1]
r1 = int(input("右边的选手:"))
r = 0
rd = base_data[r1 - 1]
number = int(input("模拟次数："))
for i in range(number):
    left_data =[]
    right_data = []
    for n1 in base_data[l1-1]:
        left_data.append(n1)
    for n2 in base_data[r1-1]:
        right_data.append(n2)
    print("战斗开始——————————————————————————————————————————————")
    n = start(left_data, right_data)
    if n=="r" :
        r=r+1
    elif n=="l":
        l=l+1

print("胜利次数：", ld[0], l, rd[0], r)

