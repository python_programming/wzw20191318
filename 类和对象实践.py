"""
    作者：王泽文
    文件名：类和对象实践
    时间：2020年4月8日
"""


class Student:
    """学生类"""
    school = "电科院"

    def __init__(self, name, age, study):
        Student.name = name
        Student.age = age
        Student.study = study
        print("我是" + Student.school + Student.study + "专业的大学生，我叫" + Student.name + "，我今年" + str(Student.age) + "岁。")

    def hobby(self):
        h = "学习"
        print("我喜欢" + h)


class Man(Student):
    """男生类"""

    def __init__(self, name, age, study):
        Student.name = name
        Student.age = age
        Student.study = study
        print("我是" + Student.school + Student.study + "专业的男生，我叫" + Student.name + "，我今年" + str(Student.age) + "岁。")

    def hobby(self):
        h = "打打游戏"
        print("我喜欢" + h)


class Woman(Student):
    """女生类"""

    def __init__(self, name, age, study):
        Student.name = name
        Student.age = age
        Student.study = study
        print("我是" + Student.school + Student.study + "专业的女生，我叫" + Student.name + "，我今年" + str(Student.age) + "岁。")

    def hobby(self):
        h = "唱歌跳舞"
        print("我喜欢" + h)


student0 = Student("张三", 19, "信息安全")
student0.hobby()
student1 = Man("李四", 20, "信息安全")
student1.hobby()
student2 = Woman("小红", 18, "管理")
student2.hobby()